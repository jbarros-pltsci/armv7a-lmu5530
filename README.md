# Cross-compiling libs to armv7a-lmu5530 (i.MX6)

## Pre-requirements
* Docker and git installed on your machine

## Download and run docker container
1. Clone this repo using `git clone https://github.com/jairobjunior/armv7a-lmu5530.git`
2. Inside of `armv7a-lmu5530` that was just created, run the command `docker build -t linux-armv7a-lmu5530 .`
3. Init a docker container by running `docker run -it -d -v <your-work-path>:/work <hash-of-image> /bin/bash`
4. Attach container to terminal `docker exec -it <hash-of-container> /bin/bash`

## Double check if your enviromental variables look like this:

run `export` on terminal

```
declare -x AR="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-ar"
declare -x ARCH="arm"
declare -x AS="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-as"
declare -x CC="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-gcc"
declare -x CMAKE_TOOLCHAIN_FILE="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/Toolchain.cmake"
declare -x CPP="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-cpp"
declare -x CROSS_COMPILE="arm-cortexa8_neon-linux-gnueabihf-"
declare -x CROSS_ROOT="/usr/xcc/arm-openwrt-linux-uclibcgnueabi"
declare -x CROSS_TRIPLE="arm-openwrt-linux-uclibcgnueabi"
declare -x CXX="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-g++"
declare -x DEFAULT_DOCKCROSS_IMAGE="dockcross/linux-armv7a:latest"
declare -x FC="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-gfortran"
declare -x GREP_COLOR="01;32"
declare -x HISTSIZE="3000"
declare -x HOME="/root"
declare -x HOSTNAME="eda1c45b52df"
declare -x LD="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin/arm-openwrt-linux-uclibcgnueabi-ld"
declare -x LESS=" -iJr"
declare -x LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=01;05;37;41:mi=01;05;37;41:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arj=01;31:*.taz=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lz=01;31:*.xz=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.jpg=01;35:*.jpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.axv=01;35:*.anx=01;35:*.ogv=01;35:*.ogx=01;35:*.pdf=00;32:*.ps=00;32:*.txt=00;32:*.patch=00;32:*.diff=00;32:*.log=00;32:*.tex=00;32:*.doc=00;32:*.aac=00;36:*.au=00;36:*.flac=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.axa=00;36:*.oga=00;36:*.spx=00;36:*.xspf=00;36:"
declare -x OLDPWD
declare -x PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/xcc/arm-cortexa8_neon-linux-gnueabihf/bin"
declare -x PKG_CONFIG_PATH="/usr/lib/arm-linux-gnueabihf/"
declare -x PWD="/work"
declare -x QEMU_LD_PREFIX="/usr/xcc/arm-openwrt-linux-uclibcgnueabi/arm-openwrt-linux-uclibcgnueabi/sysroot"
declare -x QEMU_SET_ENV="LD_LIBRARY_PATH=/usr/xcc/arm-openwrt-linux-uclibcgnueabi/lib:/usr/xcc/arm-openwrt-linux-uclibcgnueabi/arm-openwrt-linux-uclibcgnueabi/sysroot"
declare -x SHLVL="1"
declare -x STAGING_DIR="/usr/xcc/arm-openwrt-linux-uclibcgnueabi"
declare -x TERM="xterm"
declare -x XCC_PREFIX="/usr/xcc"
```

## Using ./configure, make and make install
1. After you download or clone the lib you want to compile, follow their steps of how to compile it. Every lib might have a slightly different way of compiling it. If the lib provides a `./configure` way of doing it, you will need to use with `--host` and `--prefix`, like this: `./configure --host=arm-linux --prefix=/usr/xcc/arm-openwrt-linux-uclibcgnueabi`
2. Once the configure is done you can run `make -j4`. This will make a binary file of the lib or application you are trying to use.
3. If you are compiling a dependecy of an application, you will need to use `make install` to copy that binary created into `/usr/xcc/arm-openwrt-linux-uclibcgnueabi/bin`.
